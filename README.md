# Notes on computer programming languages

[[_TOC_]]

# Language Building
* *Build Your Own Programming Language*
  2021 Clinton L. Jeffery (Packt Publishing)

# Language independant principles of programming
## API Design
* [*Common API mistakes and how to avoid them*
  ](https://blog.logrocket.com/common-api-mistakes-and-how-to-avoid-them-804fbcb9cc4b/)
  2019-01 Thomas Hunter II

## Coupling
* [*A simple recipe for framework decoupling*
  ](https://matthiasnoback.nl/2020/09/simple-recipe-for-framework-decoupling/)
  2020-06 Matthias Noback
* [decoupling programming
  ](https://google.com/search?q=decoupling+programming)

## Architecture (software design pattern)
### Event-Driven Architecture (EDA)
* [*Event-driven architecture*
  ](https://en.wikipedia.org/wiki/Event-driven_architecture)
* [*What Is Event-Driven Architecture?*
  ](https://www.confluent.io/learn/event-driven-architecture)
---
* [*Unlock the Secrets of Symfony’s Kernel Events: What Every Developer Must Know!*
  ](https://medium.com/@skowron.dev/unlock-the-secrets-of-symfonys-kernel-events-what-every-developer-must-know-7ec39f3fc003)
  2024-05 Jakub Skowron (skowron.dev, Medium)
  * But Symfony is itself a call-back for the event-driven webserver!
* [*Callback in an Event-Driven Architecture*
  ](https://medium.com/@robertoperezrodriguez_37307/callback-in-an-event-driven-architecture-7fe5d4043d5b)
  2023-09 Roberto Pérez Rodríguez
* Book: [*Functional Event-Driven Architecture*
  ](https://leanpub.com/feda)
  2023-01 Gabriel Volpe (Leanpub)
  * [github.com/gvolpe/trading](https://github.com/gvolpe/trading)
* [*Journey to Event Driven – Part 1: Why Event-First Programming Changes Everything*
  ](https://www.confluent.io/blog/journey-to-event-driven-part-1-why-event-first-thinking-changes-everything)
  2019-01 Neil Avery

### Event-driven architecture and reactive programming
* [*Event-driven architecture and reactive programming*
  ](https://retool.com/blog/event-driven-architecture-and-reactive-programming)
  2022-01 Jesse Martin
* [*How is reactive programming different than event-driven programming?*
  ](https://stackoverflow.com/questions/34495117/how-is-reactive-programming-different-than-event-driven-programming)
  (2021) (stackoverflow)
* [*Event Driven and Functional Reactive Programming, Step by Step*
  ](https://blogg.bekk.no/event-driven-and-functional-reactive-programming-step-by-step-babbd8364150?gi=16f602a99c79)
  2013-10 Stian Veum Møllersen (Medium)

# Classification and comparison of programming languages
## Introduction and [Programming paradigms](https://en.m.wikipedia.org/wiki/Programming_paradigm)
* [*Comparison of programming languages*
  ](https://en.m.wikipedia.org/wiki/Comparison_of_programming_languages)
* [*Comparison of multi-paradigm programming languages*
  ](https://en.m.wikipedia.org/wiki/Comparison_of_multi-paradigm_programming_languages)
* stack**overflow** [*Annual Developer Survey*
  ](https://insights.stackoverflow.com/survey)
* [*These Modern Programming Languages Will Make You Suffer*
  ](https://betterprogramming.pub/modern-languages-suck-ad21cbc8a57c)
  2020-12 Ilya Suzdalnitski
  * Elixir
    * Best Language for Building Web APIs
    * Best Language for Building Concurrent and Distributed Software
  * ReasonML
    * Best Frontend Language
  * F#
    * Best Language for Fintech
    * Best Language for Enterprise Software
  * Elm
  * Scala
  * OCaml
  * Haskell
  ---
  * JavaScript
  * Rust
  * Go
    * Best System Language
  * TypeScript
  * Python
    * [Redation note: Better use Julia for data science, if possible]
  * C#
  * Java
  * C++
* [*Interview with Jesper Louis Andersen about Erlang, Haskell, OCaml, Go, Idris, the JVM, software and protocol design — PART I*
  ](https://notamonadtutorial.com/interview-with-jesper-louis-andersen-about-erlang-haskell-ocaml-go-idris-the-jvm-software-and-b0de06440fbd?#.4gidstolk)
  2015-12 unbalancedparentheses
* [*Gradual typing*](https://en.m.wikipedia.org/wiki/Gradual_typing)
* [*Async/await*](https://en.m.wikipedia.org/wiki/Async/await)
* [rosettacode.org](https://rosettacode.org/)
  * [*Language Comparison Table*
    ](https://rosettacode.org/wiki/Language_Comparison_Table) - Rosetta Code
  * [Category:Programming Languages](http://rosettacode.org/wiki/Category:Programming_Languages)
  * [Pages (tasks) with the most categories](http://rosettacode.org/wiki/Special:MostCategories)
  * [*Rosetta Code*](https://en.m.wikipedia.org/wiki/Rosetta_Code)
  * [*A Comparative Study of Programming Languages in Rosetta Code*
    ](https://ieeexplore.ieee.org/abstract/document/7194625/similar)
    2015-05  Sebastian Nanz; Carlo A. Furia 
* [*PLEAC - Programming Language Examples Alike Cookbook*
  ](http://pleac.sourceforge.net)

## Comparing code (Rosetta Code)
* [Abstract type
  ](https://rosettacode.org/wiki/Abstract_type)
* [Environment variables
  ](https://rosettacode.org/wiki/Environment_variables)
* [Function composition
  ](https://www.rosettacode.org/wiki/Function_composition)
* [Higher-order functions
  ](https://rosettacode.org/wiki/Higher-order_functions)
* [Overloaded operators
  ](https://www.rosettacode.org/wiki/Overloaded_operators)
* [User defined pipe and redirection operators
  ](https://www.rosettacode.org/wiki/User_defined_pipe_and_redirection_operators)
* [Web assembly
  ](https://webassembly.org/getting-started/developers-guide/)
* [Web server
  ](https://rosettacode.org/wiki/Hello_world/Web_server)

| Language | Abstract type | Env. var. | Higher-order f. | Web server
| - | - | - | - | -
| Ada | [OK][Abstract_type#Ada] | [OK][Environment_variables#Ada] | [OK][Higher-order_functions#Ada] | [OK][Web_server#Ada]
| C | [OK][Abstract_type#C] | [OK][Environment_variables#C] | [OK][Higher-order_functions#C] | [OK][Web_server#C]


[Abstract_type#Ada]:
  https://rosettacode.org/wiki/Abstract_type#Ada
  (Abstract type)
[Environment_variables#Ada]:
  https://www.rosettacode.org/wiki/Environment_variables#Ada
  (Environment variables)
[Higher-order_functions#Ada]:
  https://www.rosettacode.org/wiki/Higher-order_functions#Ada
  (Higher-order functions)
[Web_server#Ada]:
  https://www.rosettacode.org/wiki/Hello_world/Web_server#Ada
  (Web server)

[Abstract_type#C]:
  https://rosettacode.org/wiki/Abstract_type#C
  (Abstract type)
[Environment_variables#C]:
  https://www.rosettacode.org/wiki/Environment_variables#C
  (Environment variables)
[Higher-order_functions#C]:
  https://www.rosettacode.org/wiki/Higher-order_functions#C
  (Higher-order functions)
[Web_server#C]:
  https://www.rosettacode.org/wiki/Hello_world/Web_server#C
  (Web server)




---
| C_sharp
  | [KO?](https://rosettacode.org/wiki/Abstract_type#C_sharp)
  | [KO?](https://www.rosettacode.org/wiki/Environment_variables#C_sharp)
  | [KO?](https://www.rosettacode.org/wiki/Higher-order_functions#C_sharp)
  | [?](https://www.rosettacode.org/wiki/Hello_world/Web_server#C_sharp)

| C++
  | [OK](https://rosettacode.org/wiki/Abstract_type#C%2B%2B)
  | [OK](https://www.rosettacode.org/wiki/Environment_variables#C%2B%2B)
  | [OK](https://www.rosettacode.org/wiki/Higher-order_functions#C%2B%2B)
  | [OK](https://www.rosettacode.org/wiki/Hello_world/Web_server#C%2B%2B)

| Crystal
  | [OK](https://rosettacode.org/wiki/Abstract_type#Crystal)
  | [?](https://www.rosettacode.org/wiki/Environment_variables#Crystal)
  | [?](https://www.rosettacode.org/wiki/Higher-order_functions#Crystal)
  | [OK](https://www.rosettacode.org/wiki/Hello_world/Web_server#Crystal)

| Delphi
  | [OK](https://rosettacode.org/wiki/Abstract_type#Delphi)
  | [KO?](https://www.rosettacode.org/wiki/Environment_variables#Delphi)
  | [OK](https://www.rosettacode.org/wiki/Higher-order_functions#Delphi)
  | [OK](https://www.rosettacode.org/wiki/Hello_world/Web_server#Delphi)

| Eiffel
  | [OK](https://rosettacode.org/wiki/Abstract_type#Eiffel)
  | [OK](https://www.rosettacode.org/wiki/Environment_variables#Eiffel)
  | [KO](https://www.rosettacode.org/wiki/Higher-order_functions#Eiffel)
  | [KO](https://www.rosettacode.org/wiki/Hello_world/Web_server#Eiffel)

| Elixir
  | 
  [OK: elixir-lang.org](https://elixir-lang.org/getting-started/protocols.html) Protocols
  [~Rosetta~](https://rosettacode.org/wiki/Abstract_type#Elixir)
  | [OK](https://www.rosettacode.org/wiki/Environment_variables#Elixir)
  | [OK](https://www.rosettacode.org/wiki/Higher-order_functions#Elixir)
  | [KO?](https://www.rosettacode.org/wiki/Hello_world/Web_server#Elixir)

| F_Sharp
  | [?](https://rosettacode.org/wiki/Abstract_type#F_Sharp)
  | [?](https://www.rosettacode.org/wiki/Environment_variables#F_Sharp)
  | [?](https://www.rosettacode.org/wiki/Higher-order_functions#F_Sharp)
  | [?](https://www.rosettacode.org/wiki/Hello_world/Web_server#F_Sharp)

| Go
  | [OK](https://rosettacode.org/wiki/Abstract_type#Go)
  | [OK](https://www.rosettacode.org/wiki/Environment_variables#Go)
  | [OK](https://www.rosettacode.org/wiki/Higher-order_functions#Go)
  | [OK](https://www.rosettacode.org/wiki/Hello_world/Web_server#Go)

| Haskell
  | [OK](https://rosettacode.org/wiki/Abstract_type#Haskell)
  | [OK](https://www.rosettacode.org/wiki/Environment_variables#Haskell)
  | [OK](https://www.rosettacode.org/wiki/Higher-order_functions#Haskell)
  | [OK](https://www.rosettacode.org/wiki/Hello_world/Web_server#Haskell)

| Julia
  | [OK](https://rosettacode.org/wiki/Abstract_type#Julia)
  | [OK](https://www.rosettacode.org/wiki/Environment_variables#Julia)
  | [OK](https://www.rosettacode.org/wiki/Higher-order_functions#Julia)
  | [OK](https://www.rosettacode.org/wiki/Hello_world/Web_server#Julia)

| Objective-C
  | [KO](https://rosettacode.org/wiki/Abstract_type#Objective-C)
  | [OK](https://www.rosettacode.org/wiki/Environment_variables#Objective-C)
  | [KO](https://www.rosettacode.org/wiki/Higher-order_functions#Objective-C)
  | [KO](https://www.rosettacode.org/wiki/Hello_world/Web_server#Objective-C)

| OCaml
  | [OK](https://rosettacode.org/wiki/Abstract_type#OCaml)
  | [OK](https://www.rosettacode.org/wiki/Environment_variables#OCaml)
  | [OK](https://www.rosettacode.org/wiki/Higher-order_functions#OCaml)
  | [OK](https://www.rosettacode.org/wiki/Hello_world/Web_server#OCaml)

| Perl
  | [OK](https://rosettacode.org/wiki/Abstract_type#Perl)
  | [OK](https://www.rosettacode.org/wiki/Environment_variables#Perl)
  | [OK](https://www.rosettacode.org/wiki/Higher-order_functions#Perl)
  | [OK](https://www.rosettacode.org/wiki/Hello_world/Web_server#Perl)

| Raku
  | [OK](https://rosettacode.org/wiki/Abstract_type#Raku)
  | [OK](https://www.rosettacode.org/wiki/Environment_variables#Raku)
  | [OK](https://www.rosettacode.org/wiki/Higher-order_functions#Raku)
  | [OK](https://www.rosettacode.org/wiki/Hello_world/Web_server#Raku)
[raku.org:operators]:
  https://docs.raku.org/language/optut
  (Creating operators: A short tutorial on how to declare operators and create new ones.)

| Ruby
  | [OK](https://rosettacode.org/wiki/Abstract_type#Ruby)
  | [OK](https://www.rosettacode.org/wiki/Environment_variables#Ruby)
  | [OK](https://www.rosettacode.org/wiki/Higher-order_functions#Ruby)
  | [OK](https://www.rosettacode.org/wiki/Hello_world/Web_server#Ruby)

| Rust
  | [OK](https://rosettacode.org/wiki/Abstract_type#Rust)
  | [OK](https://www.rosettacode.org/wiki/Environment_variables#Rust)
  | [OK](https://www.rosettacode.org/wiki/Higher-order_functions#Rust)
  | [OK](https://www.rosettacode.org/wiki/Hello_world/Web_server#Rust)

| Scala
  | [OK](https://rosettacode.org/wiki/Abstract_type#Scala)
  | [OK](https://www.rosettacode.org/wiki/Environment_variables#Scala)
  | [OK](https://www.rosettacode.org/wiki/Higher-order_functions#Scala)
  | [OK](https://www.rosettacode.org/wiki/Hello_world/Web_server#Scala)

| Smalltalk
  | [OK](https://rosettacode.org/wiki/Abstract_type#Smalltalk)
  | [OK](https://www.rosettacode.org/wiki/Environment_variables#Smalltalk)
  | [OK](https://www.rosettacode.org/wiki/Higher-order_functions#Smalltalk)
  | [OK](https://www.rosettacode.org/wiki/Hello_world/Web_server#Smalltalk)

| SPARK Ada
  | [?](https://rosettacode.org/wiki/Abstract_type#SPARK)
  | [?](https://www.rosettacode.org/wiki/Environment_variables#SPARK)
  | [?](https://www.rosettacode.org/wiki/Higher-order_functions#SPARK)
  | [?](https://www.rosettacode.org/wiki/Hello_world/Web_server#SPARK)

| Swift
  | [KO](https://rosettacode.org/wiki/Abstract_type#Swift)
  | [OK](https://www.rosettacode.org/wiki/Environment_variables#Swift)
  | [OK](https://www.rosettacode.org/wiki/Higher-order_functions#Swift)
  | [KO](https://www.rosettacode.org/wiki/Hello_world/Web_server#Swift)

| 
  | [OK](https://rosettacode.org/wiki/Abstract_type#)
  | [OK](https://www.rosettacode.org/wiki/Environment_variables#)
  | [OK](https://www.rosettacode.org/wiki/Higher-order_functions#)
  | [OK](https://www.rosettacode.org/wiki/Hello_world/Web_server#)
  |

```json:table
{
    "fields" : [
        {"key": "language", "label": "Language", "sortable": true},
        {"key": "Abstract_type", "label": "Abstract type", "sortable": true},
        {"key": "Higher-order_functions", "label": "Higher-order functions", "sortable": true},
        {"key": "hfp2017", "label": "Work hours per Function Point (2017)", "sortable": true}
    ],
    "items" : [
      {
      "language": "Ada",
      "Abstract_type": "OK",
      "Higher-order_functions": "OK",
      "hfp2017": 11.95
      },
      {
      "language": "Agda",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "C",
      "Abstract_type": "Work around",
      "Higher-order_functions": ""
      },
      {
      "language": "C#",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "C++",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "Clojure",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "COBOL",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "Common Lisp",
      "Abstract_type": "Work around",
      "Higher-order_functions": ""
      },
      {
      "language": "Crystal",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "D",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "Delphi",
      "Abstract_type": "OK",
      "Higher-order_functions": "",
      "hfp2017": 8.29
      },
      {
      "language": "Elixir",
      "Abstract_type": "",
      "Higher-order_functions": "",
      "hfp2017": 9.84
      },
      {
      "language": "Eiffel",
      "Abstract_type": "OK",
      "Higher-order_functions": "",
      "hfp2017": 7.16
      },
      {
      "language": "F#",
      "Abstract_type": "OK",
      "Higher-order_functions": "",
      "hfp2017": 11.31
      },
      {
      "language": "Forth",
      "Abstract_type": "Work around",
      "Higher-order_functions": ""
      },
      {
      "language": "Fortran 2008",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "Go",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "Groovy",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "Haskell",
      "Abstract_type": "OK",
      "Higher-order_functions": "",
      "hfp2017": 9.84
      },
      {
      "language": "Java",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "Julia",
      "Abstract_type": "OK",
      "Higher-order_functions": "",
      "hfp2017": 9.46
      },
      {
      "language": "Kotlin",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "Lua",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "Mathematica",
      "Abstract_type": "Work around",
      "Higher-order_functions": ""
      },
      {
      "language": "MATLAB",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "Mercury",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "newLISP",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "Nim",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "OCaml",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "Objective C",
      "Abstract_type": "",
      "Higher-order_functions": "",
      "hfp2017": 7.85
      },
      {
      "language": "ObjectPascal",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "Perl",
      "Abstract_type": "OK",
      "Higher-order_functions": "",
      "hfp2017": 9.46
      },
      {
      "language": "PHP",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "Python",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "Racket",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "Raku",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "ReScript",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "Ruby",
      "Abstract_type": "OK",
      "Higher-order_functions": "",
      "hfp2017": 11.31
      },
      {
      "language": "Rust",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "Scala",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "Smalltalk",
      "Abstract_type": "OK",
      "Higher-order_functions": "",
      "hfp2017": 6.88
      },
      {
      "language": "Standard ML",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "Tcl",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "Vala",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "Vlang",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      },
      {
      "language": "Wren",
      "Abstract_type": "OK",
      "Higher-order_functions": ""
      }
    ],
    "filter" : true
}
```

## Most popular
* [*TIOBE Index*](https://www.tiobe.com/tiobe-index)
* [RedMonk](https://redmonk.com)
* *IEEE Spectrum’s annual rankings*
  * [2024](https://spectrum.ieee.org/top-programming-languages-2024)

## Loved by users
* [*Most Loved, Dreaded, and Wanted Languages*
  ](https://insights.stackoverflow.com/survey/2019#technology-_-most-loved-dreaded-and-wanted-languages)
  2019 Stack Overflow
* https://www.slant.co/
  * [*What are the best (productivity-enhancing, well-designed, and concise, rather than just popular or time-tested) programming languages?*
    ](https://www.slant.co/topics/5984/~productivity-enhancing-well-designed-and-concise-rather-than-just-popular-or-time-tested-programming-lang)
    * Nim
    * Elixir
    * Smalltalk
    * Python
    * Haskell
    * Rust
  * [*What are the best server side programming languages?*
    ](https://www.slant.co/topics/1565/~best-server-side-programming-languages)
    * Elixir
    * Clojure
    * Python
    * Rust
    * Go
    * Haskell

## Most productive
* [*Smalltalk’s Proven Productivity*
  ](https://medium.com/smalltalk-talk/smalltalk-s-proven-productivity-fe7cbd99c061)
  2015 Richard Kenneth Eng
* [*Software Economics and Function Point Metrics: Thirty years of IFPUG Progress*
  ](https://onedrive.live.com/?authkey=!AN0R2l10YBduHY4&cid=D55F4B6296D7B073&id=D55F4B6296D7B073!43462&parId=root&o=OneUp)
  2017 Namcook Analytics LLC (Table 16)
  * [Software Economics and Function Point Metrics](https://google.com/search?q=Software+Economics+and+Function+Point+Metrics)

Some of the most productive languages (Work hours per FP)
* Smalltalk (6.88)
* Eiffel (7.16) ... Lisaac? Dafny?
* Objective C (7.85) ... Swift?
* Delphi (8.29) ... Free Pascal?
* (9.46)
  * Julia
  * Perl ... Raku?
* (9.84)
  * Haskell
  * Elixir
* (11.31)
  * Ruby ... Crystal?
  * F# ... Ocaml? Scala? Kotlin?
* Ada 95 (11.95)
* C# (12.31) ... Vala??
* (12.70)
  * Python
  * PHP
  * Java
  * Go
  * C++
* (14.64)
  * SH (shell scripts)
  * Prolog
  * Lisp
  * haXe
  * Forth
* Javascript (15.93)
* Modula (17.55)
* Pascal (19.62)
* Fortran (22.39)
* C (26.27)
* Macro Assembly (41.79)

![Most productive languages](https://kroki.io/vegalite/svg/eNqllVFvmzAQx9_zKZC3t6YO0CSEvlXZuqra1GjVpErVHi5wYDcGMzBpoyjffTaZEtwnl70g-3S___nO3Hk_8jzyuUkYFkCuPcKUqprryWSLOdCcK9auKZeTo0NnvRRc4WQ7oy-NLMnY8Ax5zpTG577fGVJQRm2v13q3BdFio_fP3d7z9sYBTbjQD6JLf0rGHhFQ5i3k2kweCxBCgdgY-yursspo08XiMHZU-MqzDEUfj2gwd8Yf1i-YKL5Fb2lrLGbOGl9QVIz38QUNY2f8vhUc-nRMp-4JrLAWg-E7aDYo3vGLqXvxBX_j9WD8Z7ve9eEgoFeBM337aTh7k4IXz97xsfuVL-3Y4Udir3aKmX6y-Mh35-9Ww-F72MJw-psczi4vLobDj8xip3Tu_petailkPpz_zptqOM3gCYfTt7JW_5G6ue0mqXmlLI0Zja-cNX7ItBX2PxPRmXurrPQJwBoxQUzn4YdKUIPVL6HuN_f5ag32cE7DyD13SGrp3TQNFmthjappQKP40Mn81t9OkRRQb86PodpVnbTgJZJjRFJJXqqTizZkXAhMtSUD0eC4bzbsK9NPMOmsh1MYLBOZ8jI_h3ozS6J4gb9KbvTJDqGbyxlHYeSPiY7Pp1JYVLLWF_OvGGTXaZz8j4n2gD8tlIorMI_lCUp0a9U2eCpfjy1lwUsTy-QwOoz-AtCTOXk=)

Made at:
* [Vega Editor](https://vega.github.io/editor)
* [kroki.io](https://kroki.io)

```vega
{
  "$schema": "https://vega.github.io/schema/vega-lite/v5.json",
  "height": 600,
  "data": {
    "values": [
      {"date": "2017-04", "language":"Smalltalk", "whpfp": 6.88},
      {"date": "2017-04", "language":"Eiffel", "whpfp": 7.16},
      {"date": "2017-04", "language":"Objective C", "whpfp": 7.85},
      {"date": "2017-04", "language":"Delphi", "whpfp": 8.29},
      {"date": "2017-04", "language":"Julia", "whpfp": 9.46},
      {"date": "2017-04", "language":"Perl", "whpfp": 9.46},
      {"date": "2017-04", "language":"Haskell", "whpfp": 9.84},
      {"date": "2017-04", "language":"Elixir", "whpfp": 9.84},
      {"date": "2017-04", "language":"Ruby", "whpfp": 11.31},
      {"date": "2017-04", "language":"F#", "whpfp": 11.31},
      {"date": "2017-04", "language":"Ada 95", "whpfp": 11.95},
      {"date": "2017-04", "language":"C#", "whpfp": 12.31},
      {"date": "2017-04", "language":"Python", "whpfp": 12.70},
      {"date": "2017-04", "language":"PHP", "whpfp": 12.70},
      {"date": "2017-04", "language":"Java", "whpfp": 12.70},
      {"date": "2017-04", "language":"Go", "whpfp": 12.70},
      {"date": "2017-04", "language":"C++", "whpfp": 12.70},
      {"date": "2017-04", "language":"Sh", "whpfp": 14.64},
      {"date": "2017-04", "language":"Prolog", "whpfp": 14.64},
      {"date": "2017-04", "language":"Lisp", "whpfp": 14.64},
      {"date": "2017-04", "language":"haXe", "whpfp": 14.64},
      {"date": "2017-04", "language":"Forth", "whpfp": 14.64},
      {"date": "2017-04", "language":"Javascript", "whpfp": 15.93},
      {"date": "2017-04", "language":"Modula", "whpfp": 17.55},
      {"date": "2017-04", "language":"Pascal", "whpfp": 19.62},
      {"date": "2017-04", "language":"Fortran", "whpfp": 22.39},
      {"date": "2017-04", "language":"C", "whpfp": 26.27},
      {"date": "2017-04", "language":"Macro Assembly", "whpfp": 41.79}
    ]
  },
  "mark": {
    "type": "line",
    "point": {
      "filled": false,
      "fill": "white"
    }
  },
  "encoding": {
    "x": {"timeUnit": "year", "field": "date", "type": "temporal"},
    "y": {"field": "whpfp", "type": "quantitative"},
    "color": {"field": "language", "type": "nominal"}
  }
}
```

### Software productivity bibliography
* [programming language productivity comparison
  ](https://scholar.google.ca/scholar?q=programming+language+productivity+comparison)
* Y. Li, L. Shi, J. Hu, Q. Wang and J. Zhai,
  "An Empirical Study to Revisit Productivity across Different Programming Languages,"
  *2017 24th Asia-Pacific Software Engineering Conference (APSEC)*, Nanjing, 2017,
  pp. 526-533. doi: 10.1109/APSEC.2017.60
  [scholar](https://scholar.google.ca/scholar?q=Y.+Li%2C+L.+Shi%2C+J.+Hu%2C+Q.+Wang+and+J.+Zhai%2C+%22An+Empirical+Study+to+Revisit+Productivity+across+Different+Programming+Languages%2C%22+2017+24th+Asia-Pacific+Software+Engineering+Conference+%28APSEC%29%2C+Nanjing%2C+2017%2C+pp.+526-533.+doi%3A+10.1109%2FAPSEC.2017.60)

## Top paying
* [Stack*overflow Developer Survey Results 2023*
  ](https://insights.stackoverflow.com/survey/2023#technology-top-paying-technologies)
* [Stack*overflow Developer Survey Results 2022*
  ](https://insights.stackoverflow.com/survey/2022#technology-top-paying-technologies)
* [Stack*overflow Developer Survey Results 2021*
  ](https://insights.stackoverflow.com/survey/2021#technology-top-paying-technologies)
* [Stack*overflow Developer Survey Results 2020*
  ](https://insights.stackoverflow.com/survey/2020#technology-what-languages-are-associated-with-the-highest-salaries-worldwide-global)
* [Stack*overflow Developer Survey Results 2017*
  ](https://insights.stackoverflow.com/survey/2017#top-paying-technologies)

## Functional programming
* [*Functional programming*](https://en.m.wikipedia.org/wiki/Functional_programming)
* [*Monad (functional programming)*
  ](https://en.m.wikipedia.org/wiki/Monad_(functional_programming))
  * Syntactic sugar do-notation
* [*Comparison of functional programming languages*
  ](https://en.m.wikipedia.org/wiki/Comparison_of_functional_programming_languages)
* [*Awesome Functional Programming*
  ](https://project-awesome.org/lucasviola/awesome-functional-programming)
* [*Functional Programming [Python] HOWTO*
  ](https://docs.python.org/dev/howto/functional.html)

### Books,  articles, and videos
* [Functional programming (Computer science)
  ](https://www.worldcat.org/search?q=su%3AFunctional+programming+%28Computer+science%29)
---
Pre-published:
* [*Functional Design and Architecture*
  ](https://www.manning.com/books/functional-design-and-architecture)
  (2024?) Alexander Granin
  * https://graninas.com/functional-design-and-architecture-book/
  * https://github.com/graninas/Functional-Design-and-Architecture
  * Haskell
---
* [*Functional Design: Principles, Patterns, and Practices*
  ](https://www.oreilly.com/library/view/functional-design-principles/9780138176518/)
  2023-10 Robert C. Martin (Addison-Wesley Professional)
  * Clojure
* [*What is Functional Programming?*
  ][adabeat2023]
  2023-08 Ada Beat
* [*Functional Declarative Design*
  ](https://github.com/graninas/functional-declarative-design-methodology)
  2023-08 Alexander Granin (GitHub) [Article]
* [*Functional Declarative Design: a Counterpart to Object-Oriented Design*
  ](https://www.oreilly.com/library/view/functional-declarative-design/10000MNHV2021137/)
  2021-09 Alexander Granin (Manning Publications) [Video]
* [*Grokking Simplicity : Taming Complex Software with Functional Thinking*
  ](https://www.worldcat.org/search?q=ti%3AGrokking+Simplicity&qt=advanced&dblist=638)
  2021-06 Eric Normand
* [*Functional Programming Made Easier*
  ](https://leanpub.com/fp-made-easier)
  A Step-by-Step Guide
  2021-06 Charles Scalfani
* [*Functional Design and Architecture*
  ](https://leanpub.com/functional-design-and-architecture)
  Building real programs in Haskell: application architectures, design patterns, best practices and approaches
  2020-10 Alexander Granin
* Trends in functional programming : 20th International Symposium, TFP 2019, Vancouver, BC, Canada, June 12-14, 2019, Revised Selected Papers*
  2020 International Symposium on Trends in Functional Programming, William J. Bowman, Ronald Garcia
* *Algorithms for functional programming*
  2018 John David Stone
* [*Swift functional programming*
  ](https://www.worldcat.org/search?q=ti%3ASwift+functional+programming)
  2017 Fatih Nayebi (Packt Publishing)
* [*Think Perl 6 how to think like a computer scientist*
  ](https://www.worldcat.org/search?q=ti%3AThink+Perl+6)
  2017 Laurent Rosenfeld and Allen B. Downey
  * Chapter 14. Functional Programming in Perl
* [*Perl 6 deep dive : data manipulation, concurrency, functional programming, and more*
  ](https://www.worldcat.org/search?q=ti%3APerl+6+Deep+Dive)
  2017 Andrew Shitov
* [*Functional programming : a PragPub anthology : exploring Clojure, Elixir, Haskell, Scala, and Swift*
  ](https://www.worldcat.org/title/functional-programming-a-pragpub-anthology-exploring-clojure-elixir-haskell-scala-and-swift)
  2017 Michael Swaine
* *Functional and reactive domain modeling*
  2016 Debasish Ghosh
* [*What are some limitations/disadvantages of functional programming? Where does it break down when you want to get things done?*
  ](https://www.quora.com/What-are-some-limitations-disadvantages-of-functional-programming-Where-does-it-break-down-when-you-want-to-get-things-done/answer/Tikhon-Jelvis)
  (2015) Tikhon Jelvis
* *Lightweight semiformal time complexity analysis for purely functional data structures*
  2008 Nils Anders Danielsson (Article)
* *Trends in Functional Programming. Volume 7*
  2007 Henrik Nilsson
* [*Everything Your Professor Failed to Tell You About Functional Programming*](https://www.linuxjournal.com/article/8850)
  2006-01 Shannon Behrens (Linux Journal)
* *Purely functional data structures*
  1999 Pieter Hartel
* *Purely functional data structures*
  1998 Chris Okasaki
* *Inductive benchmarking for purely functional data structures*
  Graeme E. Moss, Colin Runciman

[adabeat2023]:
  https://adabeat.com/fp/what-is-functional-programming/
  (Functional programming means combining functions to construct a program. Functional programming is declarative, it describes what to solve in contrast to imperative programming that describes how to solve it. In this blog post we will explain what functional programming is and how to apply functional programming concepts with examples in Erlang and one in Haskell.)

### Dependency injection
* [*Dependency injection*
  ](https://en.m.wikipedia.org/wiki/Dependency_injection)
  WikipediA
* [*Six approaches to dependency injection*
  ](https://fsharpforfunandprofit.com/posts/dependencies/) (F#)
  2020-12 ScottW
  * [serie's table of contents](https://fsharpforfunandprofit.com/posts/dependencies/#series-toc)
* [*Dependency injection in functional programming*
  ](https://dev.to/psfeng/dependency-injection-in-functional-programming-3gg4) (Kotlin)
  2019-01 Pin-Sho Feng
* [*A Functional Alternative to Dependency Injection in C++*
  ](https://accu.org/journals/overload/25/140/pamudurthy_2403/)
  2017-08 Satprem Pamudurthy (Overload, 25(140):22-24)
* [*Functional approaches to dependency injection*
  ](https://fsharpforfunandprofit.com/posts/dependency-injection-1/) (F#)
  2016-12 ScottW
  * superceded by [*Six approaches to dependency injection*
  ](https://fsharpforfunandprofit.com/posts/dependencies/)
* [*Injecting Dependencies, Partially Applying Functions, and It Really Doesn't Matter*
  ](https://blog.thecodewhisperer.com/permalink/injecting-dependencies)
  2015-11 J. B. Rainsberger
* [*Composing bigger programs: combinators*
  ](https://suave.io/composing.html)
  * Suave is a simple web development F# library providing a lightweight web server and a set of combinators to manipulate route flow and task composition.

### Dependent types and Type-Driven Development
#### Books
* [*Type-Driven Development with Idris*
  ](https://www.manning.com/books/type-driven-development-with-idris)
  2017 Edwin Brady
  * [Type-Driven Development with Idris](https://worldcat.org/search?q=Type-Driven+Development+with+Idris)

#### Other documents
* [*Dependent type*
  ](https://en.m.wikipedia.org/wiki/Dependent_type) (WikipediA)
---
* [*Dependent types and Idris*
  ](https://functional.christmas/2020/9)
  2020-12 Isak Sunde Singh
* [*A Gentle Introduction to Dependent Types*
  ](https://research.metastate.dev/a-gentle-introduction-to-dependent-types)
  2020-08 Metastate Team

### First-class and higher-order functions ###
* [*First-class function*
  ](https://en.wikipedia.org/wiki/First-class_function)
  (WikipediA)
  * Contains a table of language support
* [Higher-order_functions](https://rosettacode.org/wiki/Higher-order_functions)

### Functional Domain Modeling (cf. Domain-Driven Design, DDD)
* [*Domain modeling made functional : tackle software complexity with domain-driven design and F♯*
  ](https://worldcat.org/search?q=ti%3ADomain+modeling+made+functional+%3A+tackle+software+complexity+with+domain-driven+design+and+F%E2%99%AF)
  2018
* *Functional and Reactive Domain Modeling*
  2016-10 Debasish Ghosh (Manning)

### Garbage collection
* [functional programming without garbage collection](https://www.google.com/search?q=functional+programming+without+garbage+collection)
  * [*How does functional programming work for language without garbage collection?*
    ](https://www.quora.com/How-does-functional-programming-work-for-language-without-garbage-collection)
    * Pre-Scheme (GC-free (LIFO) subset of Scheme)
    * Carp (statically typed lisp, without a GC)
    * NewLISP ("One Reference Only" memory management)
    * Dale (C in S-Exprs (but with macros))
  * [*Is it possible to design a functional language without gc?*
    ](https://www.reddit.com/r/haskell/comments/d5d13i/is_it_possible_to_design_a_functional_language/)
    * Rust
    * ATS (ML without a runtime and an optional GC via libgc)
    * Carp (statically typed lisp which takes inspiration from Rust. Bonus: it’s written in Haskell)
    * linear types

### Languages Comparisons
#### Agda and Idris

* [*Differences between Agda and Idris*
  ](https://stackoverflow.com/questions/9472488/differences-between-agda-and-idris)
  (2022) (Stack Overflow)
* [*Idris or Agda for proofs?*
  ](https://www.reddit.com/r/Idris/comments/su7bie/idris_or_agda_for_proofs_im_wondering_if_idris_2s/)
  (2022) (Reddit)
* [*Agda vs. Coq vs. Idris*
  ](https://whatisrt.github.io/dependent-types/2020/02/18/agda-vs-coq-vs-idris.html)
  2020-02 (Meta-cedille blog)
* [*What are the differences between Agda and Idris?*
  ](http://docs.idris-lang.org/en/latest/faq/faq.html#what-are-the-differences-between-agda-and-idris)
  (docs.idris-lang.org)

### Link with Lambda Calculus
* [*Lambda calculus*
  ](https://en.m.wikipedia.org/w/index.php?title=Lambda_calculus)
* [gulcii](https://hackage.haskell.org/package/gulcii): graphical untyped lambda calculus interactive interpreter
  * https://hackage.haskell.org/package/gulcii-0.3/src/doc/encoding.md
* [*Church encoding*
  ](https://en.m.wikipedia.org/wiki/Church_encoding)
  * [*Church encoding*
    ](https://blog.ploeh.dk/2018/05/22/church-encoding)
    2018-05 Mark Seemann (ploeh blog)
  * https://hackage.haskell.org/packages/search?terms=church
* [*Mogensen–Scott encoding*
  ](https://en.m.wikipedia.org/wiki/Mogensen%E2%80%93Scott_encoding)

### Monads
#### Continuation monad
* [*The reasonable effectiveness of the continuation monad*
  ](https://blog.poisson.chat/posts/2019-10-26-reasonable-continuations.html)
  2019-10 Li-yao XIA (
Lysxia, [poisson.chat](https://poisson.chat))

### State Machines
* [*Idris state machines in JavaScript apps*
  ](https://medium.com/the-web-tub/idris-state-machines-in-javascript-apps-b969e2cb6ed2)
  2018-04 Eric Corson

## Functional Reactive Programming
* [*Scala Reactive Programming*
  ](https://www.packtpub.com/product/scala-reactive-programming/9781787288645)
  2018 Rambabu Posa
  * Reactive programming (RP), Function Reactive Programming (FRP), OOP RP, Imperative RP, and Reactive Engine
* *Reactive Design Patterns*
  2017-02 Roland Kuhn with Brian Hanafee and Jamie Allen (Manning)
* [*Specification for a Functional Reactive Programming language*
  ](https://stackoverflow.com/questions/5875929/specification-for-a-functional-reactive-programming-language#)
  (2017)
* [*Functional reactive programming*
  ](https://www.worldcat.org/search?q=ti%3AFunctional+reactive+programming)
  2016 Stephen Blackheath and Anthony Jones
* [*Haskell High Performance Programming*
  ](https://www.worldcat.org/search?q=ti%3AHaskell+High+Performance+Programming)
  2016 Samuli Thomasson
  * Book with a chapter about Haskell main FRP libraries
* [*The essence and origins of FRP*
  ](https://github.com/conal/talk-2015-essence-and-origins-of-frp)
  2015-07 conal
* [*Functional reactive programming*
  ](https://en.m.wikipedia.org/wiki/Functional_reactive_programming)
  WikipediA
* Functional reactive programming tutorial
* Functional reactive programming site:worldcat.org

## Reactive programming
* [*Reactive programming*
  ](https://en.m.wikipedia.org/wiki/Reactive_programming)
  WikipediA
* [*Best Reactive Programming Books*
  ](https://programming-digressions.com/2017/08/best-reactive-programming-books.html)
  2017-08 Akram Ahmad
* [*Reactive extensions*
  ](https://en.m.wikipedia.org/wiki/Reactive_extensions)
  WikipediA
  * [ReactiveX](http://reactivex.io)
    * Elixir
      * Reactive Extensions for Elixir
        * [alfert/reaxive](https://github.com/alfert/reaxive)
    * Javascript
      * [André Staltz (@andrestaltz): You will learn RxJS at ng-europe 2016](https://www.youtube.com/watch?v=uQ1zhJHclvs) YouTube
    * PHP
      * [reactivex/rxphp](https://phppackages.org/p/reactivex/rxphp)
        * [voryx/pgasync](https://packagist.org/packages/voryx/pgasync)
    * Scala: RxScala

# Features
## Enum and sum types
* [*A survey of programming language enum support*
  ](https://github.com/Crell/enum-comparison)
  2021-02 Crell
* [*Sum Types Are Coming: What You Should Know*
  ](https://chadaustin.me/2015/07/sum-types/)
  2015-07 Chad Austin
  * ML, Haskell, F#, Scala, Rust, Swift, and Ada

## Scope (Lexical scope vs. dynamic scope)
* [*Scope (computer science)*
  ](https://en.m.wikipedia.org/wiki/Scope_(computer_science)#Lexical_scoping_and_dynamic_scoping)
  WikipediA

## Concurrent and parallel programming
## Actor
* Erlang and Scala Akka

## Software Transactional Memory
* [Software Transactional Memory](https://www.google.com/search?q=Software+Transactional+Memory)
* [*Software transactional memory*](https://en.m.wikipedia.org/wiki/Software_transactional_memory)
  * https://tracker.debian.org/pkg/ruby-concurrent
  * https://tracker.debian.org/pkg/multiverse-core (Java, Scala)
* [*Concurrency Patterns: Actors and STM*
  ](https://staticallytyped.wordpress.com/2010/01/30/concurrency-patterns-actors-and-stm/)
  2010-01
* STM vs actors
  * haskell actor implementation

## Immutability
* [*Immutable object*](https://en.m.wikipedia.org/wiki/Immutable_object#Python)

## OOP interfaces and FP type classes
* [*Type class*
  ](https://en.wikipedia.org/wiki/Type_class)
  (WikipediA)
* [Abstract_type](https://rosettacode.org/wiki/Abstract_type)
  (Rosetta Code)
* [*Difference between OOP interfaces and FP type classes*
  ](https://stackoverflow.com/questions/8122109/difference-between-oop-interfaces-and-fp-type-classes)
  (2021) @ Stack OverFlow
* [*Type classes explained*
  ](https://medium.com/@olxc/type-classes-explained-a9767f64ed2c)
  2017-02 Oleksii Avramenko

## Lazy evaluation
* [*Lazy evaluation*
  ](https://en.m.wikipedia.org/wiki/Lazy_evaluation)
* [*What is Lazy Evaluation? — Programming Word of the Day*
  ](https://medium.com/background-thread/what-is-lazy-evaluation-programming-word-of-the-day-8a6f4410053f)
  2018-09 Marin Benčević

## Memory management
### Object finalization
* [*Object finalization and cleanup: How to design classes for proper object cleanup*
  ](https://www.javaworld.com/article/2076697/object-finalization-and-cleanup.html)
  1998 Bill Venners

### Reference counting
* See swift (or python or PHP)

### Resource Acquisition Is Initialization
* [Resource acquisition is initialization (RAII)
  ](https://google.com/search?q=Resource+acquisition+is+initialization+%28RAII%29)
* [*Resource acquisition is initialization*
  ](https://en.m.wikipedia.org/wiki/Resource_acquisition_is_initialization)

### Smart Pointers
* [smart pointers](https://google.com/search?q=smart+pointers)
* [*Smart pointer*](https://en.m.wikipedia.org/wiki/Smart_pointer)

## Operator overloading
* [*Operator overloading*
  ](https://en.wikipedia.org/wiki/Operator_overloading) (WikipediA)

## Pattern matching
* [*Category:Pattern matching programming languages*
  ](https://en.wikipedia.org/wiki/Category:Pattern_matching_programming_languages)
  (WikipediA)

## Named Arguments
* See [*cplusplus*
  ](https://gitlab.com/notes-on-computer-programming-languages/cplusplus)

# Applications
## Math
* [*Math for Programmers*
  ](https://www.manning.com/books/math-for-programmers)
  3D graphics, machine learning, and simulations with Python
  2020 Paul Orland

# Libraries
* [GitHub Trending](https://github.com/trending?since=monthly)

## CGI
* [CGI tutorial](https://google.com/search?q=CGI+tutorial)
* [*CGI 101*](http://www.cgi101.com/book/)
* [*Getting Started with CGI Programming in C*](http://jkorpela.fi/forms/cgic.html)

## Database libraries
One can make 3 categories:
* SQL helpers,
* query builders (SQL dialect independent and making a base layer for ORMs), and 
* ORMs.

### SQL helpers
* Slonik (js)
### Query builders
SQL dialect independent and making a base layer for ORMs
* Knex.js
### ORMs

### Unclassified
* [downloads|184/month]
  [hasql](https://hackage.haskell.org/package/hasql): An efficient PostgreSQL driver with a flexible mapping API

### SQL into data structures
* Clojure
  * [Honey SQL](https://clojars.org/honeysql)
    * https://gitlab.com/clojure-packages-demo/honeysql
  * [HugSQL](https://www.hugsql.org/#faq-yesql)
  * [Yesql](https://github.com/krisajenkins/yesql)
* PHP
  * ...
* Postmodern (Common Lisp) https://gitlab.com/common-lisp-packages-demo/postmodern
  * cl-s-sql https://packages.debian.org/en/sid/cl-s-sql
* [lambdalite](http://quickdocs.org/lambdalite/)

## Parser combinator
* [*Parser combinator*](https://en.wikipedia.org/wiki/Parser_combinator) @WikipediA
* [*Parsec: Direct Style Monadic Parser CombinatorsFor The Real World*
  ](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/parsec-paper-letter.pdf)
  2001 Daan Leijen, Erik Meijer

### Smalltalk, Lisaac ...
### Swift
* swift parser combinators
* [*Parser Combinators in Swift*
  ](https://academy.realm.io/posts/tryswift-yasuhiro-inami-parser-combinator/)
  2016-05 Yasuhiro Inami

### Modern Pascal
### Julia, Perl, Raku
### Haskell
* [*Parsec (parser)*](https://en.wikipedia.org/wiki/Parsec_(parser))

### Elixir
* [nimble_parsec](https://hex.pm/packages/nimble_parsec)
  * [makeup](https://hex.pm/packages/makeup)
* [combine](https://hex.pm/packages/combine)

### Ruby, F#, Ocaml, Scala, Kotlin
### Ada
### Python
* [parsec](https://pypi.org/project/parsec)

## Socket (TCP, UNIX) Programming
### Peer credentials unix domain socket
* http://man7.org/linux/man-pages/man7/unix.7.html
* [*Is there a way to get the uid of the other end of a unix socket connection*
  ](https://stackoverflow.com/questions/9898961/is-there-a-way-to-get-the-uid-of-the-other-end-of-a-unix-socket-connection)
* [peer credentials unix domain socket
  ](https://google.com/search?q=peer+credentials+unix+domain+socket)
* See also
  * Elixir (~with module for peer cred)
  * Haskell
  * Java and junixsocket
  * Perl
    * [perl unix socket peer cred
      ](https://google.com/search?q=perl+unix+socket+peer+cred)
* Not yet
  * Raku (Perl6)
  * Rust

### Postgres example
* PostgreSQL Source Code getpeereid.c (on a web page at doxygen.postgresql.org)

## Web frameworks
### Benchmarks
#### TechEmpower Web Framework Benchmarks
* https://www.techempower.com/benchmarks

Some full web / full ORM links by language
##### PHP
* ![Packagist Downloads](https://img.shields.io/packagist/dm/phpmv/ubiquity)
  [phpmv/ubiquity](https://packagist.org/packages/phpmv/ubiquity)
* ![Packagist Downloads](https://img.shields.io/packagist/dm/lizhichao/one)
  [lizhichao/one](https://packagist.org/packages/lizhichao/one)
* ![Packagist Downloads](https://img.shields.io/packagist/dm/imiphp/imi)
  [imiphp/imi](https://packagist.org/packages/imiphp/imi)
* https://packagist.org/?query=laminas
* ![Packagist Downloads](https://img.shields.io/packagist/dm/laminas/laminas-mvc)
  [laminas/laminas-mvc](https://packagist.org/packages/laminas/laminas-mvc)
* ![Packagist Downloads](https://img.shields.io/packagist/dm/laravel/laravel)
  [laravel/laravel](https://packagist.org/packages/laravel/laravel)
* ![Packagist Downloads](https://img.shields.io/packagist/dm/cakephp/cakephp)
  [cakephp/cakephp](https://packagist.org/packages/cakephp/cakephp)

##### Rust
* ![Crates.io (recent)](https://img.shields.io/crates/dr/rocket)
  [rocket](https://crates.io/crates/rocket)
* ![Crates.io (recent)](https://img.shields.io/crates/dr/axum)
  [axum](https://crates.io/crates/axum)
---
* ![Crates.io (recent)](https://img.shields.io/crates/dr/diesel)
  [diesel](https://crates.io/crates/diesel)
* ![Crates.io (recent)](https://img.shields.io/crates/dr/sea-orm)
  [sea-orm](https://crates.io/crates/sea-orm)

##### Golang
* [gin-gorm](https://google.com/search?q=gin-gorm)
* [gin-gonic/gin](https://github.com/gin-gonic/gin)

##### Crystal
* [luckyframework.org](https://luckyframework.org)
* [onyxframework.com](https://onyxframework.com)

##### JavaScript and TypeScript
* ![npm-downloads (recent)](https://badgen.net/npm/dm/@feathersjs/feathers)
  [@feathersjs/feathers](https://www.npmjs.com/package/@feathersjs/feathers)
  * [feathersjs.com](https://feathersjs.com)

##### Python
* ![PyPI - Downloads](https://img.shields.io/pypi/dm/emmett)
  [emmett](https://pypi.org/project/emmett)
* [web2py.com](http://web2py.com)
  * [web2py/web2py](https://github.com/web2py/web2py) GitHub
  * [Web2py](https://en.wikipedia.org/wiki/Web2py) WikipediA

##### Lua, MoonScript
* [*Creating a Lapis Application with MoonScript*](https://leafo.net/lapis/reference/moon_getting_started.html)

##### Haskell
* [downloads|184/month]
  [hasql](https://hackage.haskell.org/package/hasql): An efficient PostgreSQL driver with a flexible mapping API

##### C++
* [ffead-cpp](https://github.com/sumeetchhetri/ffead-cpp)

# Code Editors and Integrated Development/Debugging Environments
## General ressources
* [integrated development environment site:debian.org
  ](https://www.google.com/search?q=integrated+development+environment+site:debian.org)
* [*ProgrammingApplication*](https://wiki.debian.org/ProgrammingApplication)
* [*EditorsIDEs*](https://wiki.debian.org/EditorsIDEs)

## [Atom](https://atom.io/)
* Elixir
* Haskell
* Swift

## CodeLite
* [Debian tracker](https://tracker.debian.org/pkg/codelite)

## Geany
* [Debian tracker](https://tracker.debian.org/pkg/geany)

## Visual Studio Code (also see VSCodium)
* [*Programming Languages*](https://code.visualstudio.com/docs/languages/overview)
* [Marketplace](https://marketplace.visualstudio.com)
* [*Installing Visual Studio Code*](https://wiki.debian.org/VisualStudioCode)
* Elixir
* Haskell
* Swift

## [VSCodium](https://github.com/VSCodium/vscodium)
* [vscodium-deb-rpm-repo](https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo)

## Jet Brains (Commercial)
*   [CLion](https://www.jetbrains.com/clion/features/supported-languages.html)

# Debugging
* [*Debug*](https://www.debian.org/doc/manuals/debian-reference/ch12.en.html#_debug)

## Static code analysis
* [*List of tools for static code analysis*
  ](https://en.m.wikipedia.org/wiki/List_of_tools_for_static_code_analysis)
* [*Static code analysis tools*
  ](https://www.debian.org/doc/manuals/debian-reference/ch12.en.html#_static_code_analysis_tools)

# Benchmarks Game
* [Which programming language is fastest?](https://benchmarksgame-team.pages.debian.net/benchmarksgame/)
* [*Startup time of different programming languages*
  ](https://github.com/bdrung/startup-time)
* [*Which is the fastest web framework?*
  ](https://github.com/the-benchmarker/web-frameworks)
  * web-frameworks, routers, socket-frameworks...
* [*Recursive Fibonacci Benchmark using top languages on Github*
  ](https://github.com/drujensen/fib)

# Language translators
* [extism.org](https://extism.org)
  * [~github.com/jarble/transpiler~
    ](https://github.com/jarble/transpiler)
* [github.com/jtransc/jtransc
  ](https://github.com/jtransc/jtransc)
* [github.com/metacraft-labs/py2nim
  ](https://github.com/metacraft-labs/py2nim)

# Programming patterns and anti-patterns
## Dependency injection
* [*Dependency injection*
  ](https://en.m.wikipedia.org/wiki/Dependency_injection)
  WikipediA

## Domain-Specific Language (DSL)
* [*DomainSpecificLanguage*
  ](https://www.martinfowler.com/bliki/DomainSpecificLanguage.html)
  2008-05 Martin Fowler

## Fluent interface
* [*FluentInterface*](https://www.martinfowler.com/bliki/FluentInterface.html)
  2005-12 Martin Fowler

### ...
* [*Developing a Fluent API is so cool !*
  ](https://medium.com/@ohadinho25/developing-a-fluent-api-is-so-cool-25e99cb64f7d)
  2019-05 Ohad Avenshtein
* [*Fluent Interfaces are Evil*
  ](http://ocramius.github.io/blog/fluent-interfaces-are-evil/)
  [2013-11](https://github.com/Ocramius/ocramius.github.com/blob/source/source/_posts/2013-11-07-fluent-interfaces-are-evil.md)
  Marco Pivetta

## Observer
* Functional reactive programming (FRP) replaces Observer, radically improving the quality of event-based code.
  * [*Functional Reactive Programming*
  ](https://www.manning.com/books/functional-reactive-programming) (Book description)

## Refactoring
* [*Refactoring : improving the design of existing code*
  ](https://www.worldcat.org/search?q=Refactoring+%3A+improving+the+design+of+existing+code)
  2019 Martin Fowler; Kent Beck

# Little languages
* [ATS](https://packages.debian.org/en/sid/ats2-lang)
  * ["ATS is an ML without a runtime and an optional GC via libgc."](https://www.reddit.com/r/haskell/comments/d5d13i/is_it_possible_to_design_a_functional_language/)
    * [manpages.debian.org](https://manpages.debian.org/unstable/ats2-lang)
    * https://fr.slideshare.net/mirrorren/ats-programming
* [Carp](https://github.com/carp-lang/Carp)
  A statically typed lisp, without a GC, for real-time applications. (in Haskell)
  * [repology.org](https://repology.org/project/carp)
  * [Memory Management - a closer look](https://github.com/carp-lang/Carp/blob/master/docs/Memory.md)
* Dale [tomhrr/dale](https://github.com/tomhrr/dale) (2018) Not so far from Carp.
* [*E*](https://en.wikipedia.org/wiki/E_(programming_language))
* Elm (elm/compiler)
* Fay (faylang)
* [*Inko*](https://inko-lang.org/)
* Mercury
  * [*Learn X in Y minutes, Where X=mercury*](https://learnxinyminutes.com/docs/mercury)
* [*Neko*](https://en.wikipedia.org/wiki/NekoVM)
* [newLISP](http://newlisp.org/)
  * [repology.org](https://repology.org/project/newlisp)
* [*Pony*](https://www.ponylang.io/)
* Purescript

# Mini languages
* With a package manager
* Mini version of other languages
* web application capable
* Compressed - installed sizes from Alpine Linux
---
* 65 - 160 kB lua5.3
* 150 - 480 kB jimsh (figures from Debian)
* 200 - 450 kB micropython
* 250 - 600 kB mruby
* 500 - 1000 kB luajit
* 2500 - 3300 kB Chez Scheme
